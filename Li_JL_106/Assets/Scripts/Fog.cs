﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fog : MonoBehaviour

{
    PlayerMovement pc;
    // Start is called before the first frame update
    public void Start()
    {
        gameObject.SetActive(false);
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        pc = player.GetComponent<PlayerMovement>();

    }

    // Update is called once per frame
    void Update()
    {
        if (pc.collection >= 0)
        {
            gameObject.SetActive(true);
        }
    }
}

