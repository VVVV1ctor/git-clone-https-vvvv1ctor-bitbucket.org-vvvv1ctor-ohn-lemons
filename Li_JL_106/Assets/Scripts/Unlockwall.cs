﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unlockwall : MonoBehaviour
{
    PlayerMovement pc;//reference to player
    public int collectionRequirement = 3;// How much collection do we need to unlock wall

    public void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player"); //search player gameobject with tag
        pc = player.GetComponent<PlayerMovement>();//find player movement script
    }

    // Update is called once per frame
    void Update()
    {
        if (pc.collection >= collectionRequirement)
        {
            Destroy(gameObject);//destory self 

        }
    }
}
