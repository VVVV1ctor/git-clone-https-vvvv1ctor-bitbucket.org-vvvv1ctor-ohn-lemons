﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Gmovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public KeyCode left;
    public KeyCode rifght;
    public KeyCode moveforword;
    public KeyCode moveback;


    Animator m_Animator;
    Rigidbody GhostRigidbody;
    AudioSource m_AudioSource;
    Vector3 Ghost_Movement;
    Quaternion GhostRotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        GhostRigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
       


    }
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

       

        Ghost_Movement.Set(horizontal, 0f, vertical);
        Ghost_Movement.Normalize();
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, Ghost_Movement, turnSpeed * Time.deltaTime, 0f);
        GhostRotation = Quaternion.LookRotation(desiredForward);
    }

   
}
